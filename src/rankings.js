import React, { useEffect, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
export default function Rank() {
  const [rankings, setRankings] = useState([]);
  const [rowData,setRowData]=useState([]);
  const columns =[
    {headerName:"Rank", field :"rank",sortable:true,filter:true},
    {headerName:"Country", field :"country",sortable:true,filter:true},
    {headerName:"Score", field :"score",sortable:true,filter:true},
    {headerName:"Year", field :"year",sortable:true,filter:true}
  ]

  var i = [];
  useEffect(() => {fetch("http://131.181.190.87:3000/rankings")
  .then(res => res.json())
  .then(data =>
    data.map(rank =>{
      return {
        rank:rank.rank,
        country:rank.country,
        score:rank.score,
        year:rank.year
      };
    })
    )
  .then(rank => { 
    setRowData(rank);
  })
  .catch(err => { console.log(err); 
  })});
  
  return (
  <div
    className="ag-theme-balham"
    style={{
      height:"600px",
      width:"1000px"
    }}
  >
    <AgGridReact columnDefs={columns} rowData={rowData}/>
  </div>
  );
}