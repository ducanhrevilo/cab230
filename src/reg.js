import React, { useState } from 'react';

export default function Reg() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  return (
    <div className="container">
      <h2>Create your account here</h2>
      <form
        onSubmit={event => {
          event.preventDefault();
        }}
      >
        <label htmlFor="email">Email:</label>
        <input
          type="email"
          name="email"
          id="email"
          value={email}
          onChange={event => {
            const { value } = event.target;
            setEmail(value);
          }}
        />
        <br></br><br></br>
        <label htmlFor="password">Password: </label>
        <input
          type="password"
          name="password"
          id="password"
          value={password}
          onChange={event => {
            const { value } = event.target;
            setPassword(value);
          }}
        />
        <br></br><br></br>
        <button type="submit" onClick={() => regBtn(email, password)}>Register</button>
      </form>
      <div id="app"></div>
    </div>
  );
}
function regBtn(email, password) {
    return fetch("http://131.181.190.87:3000/user/register", {
        method: "POST",
        body: JSON.stringify({ email: email, password: password }),
        headers: {
            accept: "application/json", "Content-Type": "application/json"         }
    })
        .then(res => res.json())
        .then(function (result) {
            let appDiv = document.getElementById("app");
            appDiv.innerHTML = JSON.stringify(result);
            regBtn.disabled = true;
        })
        .catch(function (error) {
            console.log(
                "There has been a problem with your fetch operation: ",
                error.message
            );
        });
}