import React, { useState } from 'react';

export default function Log() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  return (
    <div className="container">
      <h2>Welcome, please sign in!</h2>
      <form
        onSubmit={event => {
          event.preventDefault();
        }}
      >
        <label htmlFor="email">Username:</label>
        <input
          type="email"
          name="email"
          id="email"
          value={email}
          onChange={event => {
            const { value } = event.target;
            setEmail(value);
          }}
        />
        <br></br><br></br>
        <label htmlFor="password">Password: </label>
        <input
          type="password"
          name="password"
          id="password"
          value={password}
          onChange={event => {
            const { value } = event.target;
            setPassword(value);
          }}
        />
        <br></br><br></br>
        <button onClick={() => logBtn(email, password)}>Login</button>
      </form>
      
      <div id="app"></div>
    </div>
  );
}
 function logBtn(email, password) {
    fetch("http://131.181.190.87:3000/user/login", {
        method: "POST",
        body: JSON.stringify({ email: email, password: password }),
        headers: {
            accept: "application/json", "Content-Type": "application/json"         
        }
    })
        .then((res) => res.json())
        .then((res)=>{
            localStorage.setItem("token",res.token)
        })
}
function Authenticate(){
    const token = localStorage.getItem("token")
    const headers ={
        accept: "application/json", "Content-Type": "application/json", 
        Authorization : 'Bearer ${token}'        
    }
    return fetch("http://131.181.190.87:3000/route",{headers})
    .then((res) => res.json)
    .then((res) => console.log(res)

    )}
